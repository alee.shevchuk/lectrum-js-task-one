import { add } from 'date-fns';
import { saveToStorage } from './login';

const registerButton = document.getElementById("regBtn");
const createAccount =  document.getElementById("createAccount")
const stepButton = document.getElementById("toStep2Btn")
const toLoginSvg = document.getElementById("toLoginSvg");
const from3to2Svg = document.getElementById('from3to2Svg')
const from1to2 =  document.getElementById('from1to2');
const from2to1 =  document.getElementById('from2to1');

const registerBlock = document.getElementById("regBlock");
const loginBlock = document.getElementById("loginBlock");
const stepOneblock = document.getElementById("step1Block");

// forward

registerButton.onclick = () => {
   loginBlock.style.display = 'none';
   stepOneblock.style.display = 'flex';
}

stepButton.onclick = () => {
    stepOneblock.style.display = 'none';
    registerBlock.style.display = 'flex'
}

// backwords

toLoginSvg.onclick = () => {
    stepOneblock.style.display = 'none';
    loginBlock.style.display = 'flex';
}

from3to2Svg.onclick = () => {
    stepOneblock.style.display = 'flex';
    registerBlock.style.display = 'none'
}

// bullets

from1to2.onclick = () =>{
   stepOneblock.style.display = 'none';
   registerBlock.style.display = 'flex'
}

from2to1.onclick = () =>{
    stepOneblock.style.display = 'flex';
    registerBlock.style.display = 'none'
}

//  checkbox and form

const user  = {}

const userType = document.querySelector('.form-user-type');

   userType.onclick = () =>{
          
        const input = stepOneblock.querySelector('form input:checked');
        const type = input.id === 'user_student' ? 'student' : 'teacher';
        user.type = type;
    };
    createAccount.onclick = () => {
       const name = document.getElementById('name');
       const email = document.getElementById('email');
       const password = document.getElementById('password');
       const passwordNext = document.getElementById('password_next');
        
       if(password.value !== passwordNext.value){
           password.classList.add('error');
           passwordNext.classList.add('error');
           return;
       }
       
       let nameStr = name.value.split(' ');

       if(nameStr[0].length <= 3 || nameStr[1].length <=3){
           name.classList.add('error');
           return;
       }
       
       user.name = name.value;
       user.email = email.value;
       user.password = password.value;

       saveToStorage(user);

       if(user.type === 'teacher'){
           localStorage.setItem('teacher', JSON.stringify(user));
           window.location.href = user.type = 'teacher.html';
       } else {
           const students = JSON.parse(localStorage.getItem('students')) || [];

           localStorage.setItem('students', JSON.stringify([user, ...students]));
           window.location.href = user.type = 'student.html';
       }

      
   };











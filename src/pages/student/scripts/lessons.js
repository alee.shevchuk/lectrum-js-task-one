

import { id } from 'date-fns/locale';
import { timeSlots, lessons } from './constants';
import { user } from './profile';

const lesson = {}

const formLessons = document.getElementById('formLessons');
const btnSubmit = formLessons.querySelector('.button');

btnSubmit.addEventListener('click', (e) => {
    e.preventDefault()
   
    const lessonTypeBtn = formLessons.querySelector('input[name="type"]:checked');
    const lessonTimeBtn = formLessons.querySelector('input[name="time"]:checked');
    const Type = lessons[lessonTypeBtn.id];
    const Time = timeSlots[lessonTimeBtn.id];

    let day = false;

    if (lessonTimeBtn.id === 'time_04' || lessonTimeBtn.id === 'time_05' || lessonTimeBtn.id === 'time_06') {
        day = true;
    }
    
    
    lesson.name = user.name;
    lesson.time = Time;
    lesson.tomorrow = day;
    lesson.title = Type.title;
    lesson.duration = Type.duration;
    
    const savedLessons = JSON.parse(localStorage.getItem('lessons')) || [];
    localStorage.setItem('lessons', JSON.stringify([lesson, ...savedLessons]));

});






  




